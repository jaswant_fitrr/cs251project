  /*
* Copyright (c) 2006-2009 Erin Catto http://www.box2d.org
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

/* 
 * Base code for CS 251 Software Systems Lab 
 * Department of Computer Science and Engineering, IIT Bombay
 * 
 */


#include "cs251_base.hpp"
#include "render.hpp"

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
	#include "GL/freeglut.h"
#endif

#include <cstring>
#include "math.h"
 using namespace std;

#include "dominos.hpp"
#define PI 3.14159265
 namespace cs251
 {
  /**  The is the constructor 
   * This is the documentation block for the constructor.
   */ 

dominos_t::dominos_t()
{
     b2Body* b1;  

    /*! Top Horizontal Box |
    * b2PolygonShape is Set as Box |
    * Box width is 4 and height is 1 
    */
     {
      b2PolygonShape shape;
      shape.SetAsBox(4.0f, 1.0f);

      b2BodyDef bd;
      bd.position.Set(-33.0f, 40.0f);
      b2Body* ground = m_world->CreateBody(&bd);
      ground->CreateFixture(&shape, 0.0f);
    }
    /*! The Heavy Sphere on the Box |
    * b2CircleShape is used to make Sphere |
    * Sphere radius is 1.5 and density 50 |
    * Sphere is given and intial velocity of 0.5 in horizontal direction
    */
    {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 1.5;

      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 50.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(-35.0f, 45.5f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);


      b2Vec2 vel = sbody->GetLinearVelocity();
      vel.x = 0.5;
      sbody->SetLinearVelocity( vel );
    }


    /*! Horizontal Plank below small Sphere |
    * b2EdgeShape is a line segment |
    * It's Line Segment Co-ordinates are (-37, 39) to (-21, 39)
    */
    {

      b2EdgeShape shape; 
      shape.Set(b2Vec2(-37.0f, 39.0f), b2Vec2(-21.0f, 39.0f));
      b2BodyDef bd; 
      b1 = m_world->CreateBody(&bd); 
      b1->CreateFixture(&shape, 0.0f);


    }
    /*!Static Inclined Plank |
    *b2EdgeShape is set as line segment |
    *Co-ordinates of line segments are (-21,39) to (-7, 33)
    */
    {

      b2EdgeShape shape; 
      shape.Set(b2Vec2(-21.0f, 39.0f), b2Vec2(-7.0f, 33.0f));
      b2BodyDef bd; 
      b1 = m_world->CreateBody(&bd); 
      b1->CreateFixture(&shape, 0.0f);


    }
    /*!Static plank with Dominos on it |
    * b2EdgeShape is set as line segment |
    * Co-ordinates of line segments are (-7,33) to (18,33)
    */
    {

      b2EdgeShape shape; 
      shape.Set(b2Vec2(-7.0f, 33.0f), b2Vec2(18.0f, 33.0f));
      b2BodyDef bd; 
      b1 = m_world->CreateBody(&bd); 
      b1->CreateFixture(&shape, 0.0f);


    }

     /*!The Small Sphere on the platform |
     *b2CircleShape is used to make sphere |
     *Sphere radius is set to 1 and density 50
     */
    {
      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 1.0;

      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 50.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(-23.0f, 40.0f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);
    }
    

     /*!Dominos on level 1 |
     *b2PolygonShape is used to make dominos |
     *Each Domino is of width 0.3 and height 2 |
     *9 Dominos are placed in this manner 
     */
    {
      b2PolygonShape shape;
      shape.SetAsBox(0.3f, 2.0f);

      b2FixtureDef fd;
      fd.shape = &shape;
      fd.density = 60.0f;
      fd.friction = 0.5f;

      for (int i = 0; i < 9; ++i)
      {
        b2BodyDef bd;
        bd.type = b2_dynamicBody;
        bd.position.Set(-1.0f + 2.0f * i, 34.00f);
        b2Body* body = m_world->CreateBody(&bd);
        body->CreateFixture(&fd);
      }
    }


    /*! Rotating bar on level 1 |
    *b2PolygonShape is set as Box to make bar |
    *It's width is 0.3 and Height is 4.0 |
    *It rotates about the point (18.5, 32.5) 
    */
    {

      b2Body* b2;
      {
        b2PolygonShape shape;
        shape.SetAsBox(0.30f, 4.0f);

        b2FixtureDef fd;
        fd.shape = &shape;
        fd.density = 60.0f;
        fd.friction = 0.5f;

        b2BodyDef bd;
        bd.type = b2_dynamicBody;
        bd.position.Set(18.5f, 32.5f);
        b2 = m_world->CreateBody(&bd);
        b2->CreateFixture(&shape, 10.0f);
      }


      b2Body* sbody;
      b2CircleShape circle;
      circle.m_radius = 0.30;

      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 50.0f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;

      ballbd.position.Set(18.5f, 32.5f);
      sbody = m_world->CreateBody(&ballbd);
      sbody->CreateFixture(&ballfd);



      b2RevoluteJointDef jd;
      b2Vec2 anchor;
      anchor.Set(18.5f, 32.5f);
      jd.Initialize(sbody, b2, anchor);
      m_world->CreateJoint(&jd);

    }





/*******level 2*****/
    {   

   /*!Horizontal platform on level 2
   */
      b2Body* bl;
      {
        b2PolygonShape shape;
        shape.SetAsBox(25.5f, 0.01f);

        b2FixtureDef fd;
        fd.shape = &shape;
        fd.density = 60.0f;

        b2BodyDef bd;
        bd.position.Set(-5.5f, 26.99f);
        bl = m_world->CreateBody(&bd);
        bl->CreateFixture(&shape, 10.0f);
      }


       /*!Dominos on level 2 |
       *b2PolygonShape is set to Box to generate Dominos |
       *Each Domino width is 0.3 and height 2.0 
       */
      {
        b2PolygonShape shape;
        shape.SetAsBox(0.3f, 2.0f);
        
        b2FixtureDef fd;
        fd.shape = &shape;
        fd.density = 60.0f;
        fd.friction = 0.1f;

        for (int i = 0; i < 22; ++i)
        {
          b2BodyDef bd;
          bd.type = b2_dynamicBody;
          bd.position.Set(-25.0f + 2.0f * i, 29.00f);
          b2Body* body = m_world->CreateBody(&bd);
          body->CreateFixture(&fd);
        }
        shape.SetAsBox(0.3f, 2.0f);
        b2BodyDef bd;
        bd.type = b2_dynamicBody;
        bd.position.Set(-27.0f , 29.00f);
        b2Body* body = m_world->CreateBody(&bd);
        body->CreateFixture(&fd);

      }
      /*!Pendulum level 2 |
      *Rod hanging pendulum is generated using b2PolygonShape |
      *Rod width is 0.1 and height 3.0 |
      *Sphere attached at end of rod is generated using b2CircleShape |
      *Sphere radius is 1 and density 70
      */
      b2Body* b2;
      {
        b2PolygonShape shape;
        shape.SetAsBox(0.10f, 3.0f);

        b2FixtureDef fd;
        fd.shape = &shape;
        fd.density = 60.0f;
        fd.friction = 0.5f;

        b2BodyDef bd;
        bd.type = b2_dynamicBody;
        bd.position.Set(-31.0f, 30.0f);
        b2 = m_world->CreateBody(&bd);
        b2->CreateFixture(&shape, 10.0f);
      }
      

      b2Body* sbody;
      {
        b2CircleShape circle;
        circle.m_radius = 1.0;

        b2FixtureDef ballfd;
        ballfd.shape = &circle;
        ballfd.density = 70.0f;
        ballfd.friction = 0.0f;
        ballfd.restitution = 1.0f;
        b2BodyDef ballbd;
        ballbd.type = b2_dynamicBody;
        ballbd.position.Set(-31.0f, 34.0f);
        sbody = m_world->CreateBody(&ballbd);
        sbody->CreateFixture(&ballfd);
      }


      b2WeldJointDef jd;
      b2Vec2 anchor;
      anchor.Set(-31.0f, 34.0f);
      jd.Initialize(sbody, b2, anchor);
      m_world->CreateJoint(&jd);


      b2RevoluteJointDef jd1;
      b2Vec2 anchor1;
      anchor1.Set(-31.0f, 27.0f);
      jd1.Initialize(b2, bl, anchor1);
      m_world->CreateJoint(&jd1);





    /*!Five pendulums below |
    *b2PolygonShape is used to rod that holds the bob at end of pendulum |
    *Rod width is 0.1 and height is 3.0 |
    *Sphere is generated by using b2CircleShape |
    *Sphere radius is 1 and density is 50 |
    *Five identical pendulums are created side-by-side
    */
    {

     b2Body* b2;
     {
      b2PolygonShape shape;
      shape.SetAsBox(0.10f, 3.0f);

      b2FixtureDef fd;
      fd.shape = &shape;
      fd.density = 60.0f;
      fd.friction = 0.5f;

      b2BodyDef bd;
      bd.type = b2_dynamicBody;
      bd.position.Set(-27.0f, 24.0f);
      b2 = m_world->CreateBody(&bd);
      b2->CreateFixture(&shape, 10.0f);

    }


      b2Body* sbody;
      {
        b2CircleShape circle;
        circle.m_radius = 1.0;

        b2FixtureDef ballfd;
        ballfd.shape = &circle;
        ballfd.density = 50.0f;
        ballfd.friction = 1.0f;
        ballfd.restitution = 0.0f;
        b2BodyDef ballbd;
        ballbd.type = b2_dynamicBody;
        ballbd.position.Set(-27.0f, 20.0f);
        sbody = m_world->CreateBody(&ballbd);
        sbody->CreateFixture(&ballfd);
      }


    b2WeldJointDef jd;
    b2Vec2 anchor;
    anchor.Set(-27.0f, 20.0f);
    jd.Initialize(sbody, b2, anchor);
    m_world->CreateJoint(&jd);

    b2RevoluteJointDef jd1;
    b2Vec2 anchor1;
    anchor1.Set(-27.0f, 27.0f);
    jd1.Initialize(b2, bl, anchor1);
    m_world->CreateJoint(&jd1);

  }
  {

     b2Body* b2;
     {
      b2PolygonShape shape;
      shape.SetAsBox(0.10f, 3.0f);

      b2FixtureDef fd;
      fd.shape = &shape;
      fd.density = 60.0f;
      fd.friction = 0.5f;

      b2BodyDef bd;
      bd.type = b2_dynamicBody;
      bd.position.Set(-29.0f, 24.0f);
      b2 = m_world->CreateBody(&bd);
      b2->CreateFixture(&shape, 10.0f);

    }


      b2Body* sbody;
      {
        b2CircleShape circle;
        circle.m_radius = 1.0;

        b2FixtureDef ballfd;
        ballfd.shape = &circle;
        ballfd.density = 50.0f;
        ballfd.friction = 1.0f;
        ballfd.restitution = 0.0f;
        b2BodyDef ballbd;
        ballbd.type = b2_dynamicBody;
        ballbd.position.Set(-29.0f, 20.0f);
        sbody = m_world->CreateBody(&ballbd);
        sbody->CreateFixture(&ballfd);
      }


    b2WeldJointDef jd;
    b2Vec2 anchor;
    anchor.Set(-29.0f, 20.0f);
    jd.Initialize(sbody, b2, anchor);
    m_world->CreateJoint(&jd);

    b2RevoluteJointDef jd1;
    b2Vec2 anchor1;
    anchor1.Set(-29.0f, 27.0f);
    jd1.Initialize(b2, bl, anchor1);
    m_world->CreateJoint(&jd1);

  }
  {

     b2Body* b2;
     {
      b2PolygonShape shape;
      shape.SetAsBox(0.10f, 3.0f);

      b2FixtureDef fd;
      fd.shape = &shape;
      fd.density = 60.0f;
      fd.friction = 0.5f;

      b2BodyDef bd;
      bd.type = b2_dynamicBody;
      bd.position.Set(-25.0f, 24.0f);
      b2 = m_world->CreateBody(&bd);
      b2->CreateFixture(&shape, 10.0f);

    }


      b2Body* sbody;
      {
        b2CircleShape circle;
        circle.m_radius = 1.0;

        b2FixtureDef ballfd;
        ballfd.shape = &circle;
        ballfd.density = 50.0f;
        ballfd.friction = 0.0f;
        ballfd.restitution = 0.0f;
        b2BodyDef ballbd;
        ballbd.type = b2_dynamicBody;
        ballbd.position.Set(-25.0f, 20.0f);
        sbody = m_world->CreateBody(&ballbd);
        sbody->CreateFixture(&ballfd);
      }


    b2WeldJointDef jd;
    b2Vec2 anchor;
    anchor.Set(-25.0f, 20.0f);
    jd.Initialize(sbody, b2, anchor);
    m_world->CreateJoint(&jd);

    b2RevoluteJointDef jd1;
    b2Vec2 anchor1;
    anchor1.Set(-25.0f, 27.0f);
    jd1.Initialize(b2, bl, anchor1);
    m_world->CreateJoint(&jd1);

  }
  {

     b2Body* b2;
     {
      b2PolygonShape shape;
      shape.SetAsBox(0.10f, 3.0f);

      b2FixtureDef fd;
      fd.shape = &shape;
      fd.density = 60.0f;
      fd.friction = 0.5f;

      b2BodyDef bd;
      bd.type = b2_dynamicBody;
      bd.position.Set(-23.0f, 24.0f);
      b2 = m_world->CreateBody(&bd);
      b2->CreateFixture(&shape, 10.0f);

    }


      b2Body* sbody;
      {
        b2CircleShape circle;
        circle.m_radius = 1.0;

        b2FixtureDef ballfd;
        ballfd.shape = &circle;
        ballfd.density = 50.0f;
        ballfd.friction = 0.0f;
        ballfd.restitution = 0.0f;
        b2BodyDef ballbd;
        ballbd.type = b2_dynamicBody;
        ballbd.position.Set(-23.0f, 20.0f);
        sbody = m_world->CreateBody(&ballbd);
        sbody->CreateFixture(&ballfd);
      }


    b2WeldJointDef jd;
    b2Vec2 anchor;
    anchor.Set(-23.0f, 20.0f);
    jd.Initialize(sbody, b2, anchor);
    m_world->CreateJoint(&jd);

    b2RevoluteJointDef jd1;
    b2Vec2 anchor1;
    anchor1.Set(-23.0f, 27.0f);
    jd1.Initialize(b2, bl, anchor1);
    m_world->CreateJoint(&jd1);
}
{

     b2Body* b2;
     {
      b2PolygonShape shape;
      shape.SetAsBox(0.10f, 3.0f);

      b2FixtureDef fd;
      fd.shape = &shape;
      fd.density = 60.0f;
      fd.friction = 0.5f;

      b2BodyDef bd;
      bd.type = b2_dynamicBody;
      bd.position.Set(-21.0f, 24.0f);
      b2 = m_world->CreateBody(&bd);
      b2->CreateFixture(&shape, 10.0f);

    }


      b2Body* sbody;
      {
        b2CircleShape circle;
        circle.m_radius = 1.0;

        b2FixtureDef ballfd;
        ballfd.shape = &circle;
        ballfd.density = 50.0f;
        ballfd.friction = 0.0f;
        ballfd.restitution = 0.0f;
        b2BodyDef ballbd;
        ballbd.type = b2_dynamicBody;
        ballbd.position.Set(-21.0f, 20.0f);
        sbody = m_world->CreateBody(&ballbd);
        sbody->CreateFixture(&ballfd);
      }


    b2WeldJointDef jd;
    b2Vec2 anchor;
    anchor.Set(-21.0f, 20.0f);
    jd.Initialize(sbody, b2, anchor);
    m_world->CreateJoint(&jd);

    b2RevoluteJointDef jd1;
    b2Vec2 anchor1;
    anchor1.Set(-21.0f, 27.0f);
    jd1.Initialize(b2, bl, anchor1);
    m_world->CreateJoint(&jd1);
}
 
  
}
   /*!Rotating bar near level 3, After below pendulums |
   *Rotating Bar is made using b2PolygonShape |
   *Rod width is 0.3 and height is 4 |
   *Rod is fixed at (-17.5, 17.5)
   */
  {

    b2Body* b2;
    {
      b2PolygonShape shape;
      shape.SetAsBox(0.30f, 4.0f);

      b2FixtureDef fd;
      fd.shape = &shape;
      fd.density = 60.0f;
      fd.friction = 0.5f;

      b2BodyDef bd;
      bd.type = b2_dynamicBody;
      bd.position.Set(-17.5f, 17.5f);
      b2 = m_world->CreateBody(&bd);
      b2->CreateFixture(&shape, 10.0f);
    }


    b2Body* sbody;
    b2CircleShape circle;
    circle.m_radius = 0.30;

    b2FixtureDef ballfd;
    ballfd.shape = &circle;
    ballfd.density = 50.0f;
    ballfd.friction = 0.0f;
    ballfd.restitution = 0.0f;
    b2BodyDef ballbd;

    ballbd.position.Set(-17.5f, 17.5f);
    sbody = m_world->CreateBody(&ballbd);
    sbody->CreateFixture(&ballfd);



    b2RevoluteJointDef jd;
    b2Vec2 anchor;
    anchor.Set(-17.5f, 17.5f);
    jd.Initialize(sbody, b2, anchor);
    m_world->CreateJoint(&jd);

  }
   /*!Horizontal Static Shelf level 3 |
   *b2EdgeShape is used to make Static Shelf |
   *It's co-ordinates of shelf are (-14, 17.5) to (20, 17.5)
   */
  {

    b2EdgeShape shape; 
    shape.Set(b2Vec2(-14.0f, 17.5f), b2Vec2(20.0f, 17.5f));
    b2BodyDef bd; 
    b1 = m_world->CreateBody(&bd); 
    b1->CreateFixture(&shape, 0.0f);


  }

/*!Ball on the platform |
*b2CircleShape is used to create a sphere |
*Sphere has a radius of 1 and density 2
*/
  {
    b2Body* sbody;
    b2CircleShape circle;
    circle.m_radius = 1.0;

    b2FixtureDef ballfd;
    ballfd.shape = &circle;
    ballfd.density = 2.0f;
    ballfd.friction = 0.0f;
    ballfd.restitution = 0.0f;
    b2BodyDef ballbd;
    ballbd.type = b2_dynamicBody;
    ballbd.position.Set(-12.75f, 18.5f);
    sbody = m_world->CreateBody(&ballbd);
    sbody->CreateFixture(&ballfd);
  }



   //The pulley system
    {
      b2BodyDef *bd = new b2BodyDef;
      bd->type = b2_dynamicBody;
      bd->position.Set(21,14);
      bd->fixedRotation = true;
      
      //The open box
      b2FixtureDef *fd1 = new b2FixtureDef;
      fd1->density = 10.0;
      fd1->friction = 0.5;
      fd1->restitution = 0.f;
      fd1->shape = new b2PolygonShape;
      b2PolygonShape bs1;
      bs1.SetAsBox(2,0.2, b2Vec2(0.f,-1.9f), 0);
      fd1->shape = &bs1;


      b2FixtureDef *fd2 = new b2FixtureDef;
      fd2->density = 10.0;
      fd2->friction = 0.5;
      fd2->restitution = 0.f;
      fd2->shape = new b2PolygonShape;
      b2PolygonShape bs2;
      bs2.SetAsBox(0.2,2, b2Vec2(2.0f,0.f), 0);
      fd2->shape = &bs2;


      b2FixtureDef *fd3 = new b2FixtureDef;
      fd3->density = 10.0;
      fd3->friction = 0.5;
      fd3->restitution = 0.f;
      fd3->shape = new b2PolygonShape;
      b2PolygonShape bs3;
      bs3.SetAsBox(0.2,2, b2Vec2(-2.0f,0.f), 0);
      fd3->shape = &bs3;
       
      b2Body* box1 = m_world->CreateBody(bd);
      box1->CreateFixture(fd1);
      box1->CreateFixture(fd2);
      box1->CreateFixture(fd3);

      //The bar
      bd->position.Set(28,16);  
      fd1->density = 34.0;    
      b2Body* box2 = m_world->CreateBody(bd);
      box2->CreateFixture(fd1);

      // The pulley joint
      b2PulleyJointDef* myjoint = new b2PulleyJointDef();
      b2Vec2 worldAnchorOnBody1(21, 14); // Anchor point on body 1 in world axis
      b2Vec2 worldAnchorOnBody2(28, 16); // Anchor point on body 2 in world axis
      b2Vec2 worldAnchorGround1(21, 20); // Anchor point for ground 1 in world axis
      b2Vec2 worldAnchorGround2(28, 20); // Anchor point for ground 2 in world axis
      float32 ratio = 1.0f; // Define20atio
      myjoint->Initialize(box1, box2, worldAnchorGround1, worldAnchorGround2, box1->GetWorldCenter(), box2->GetWorldCenter(), ratio);
      m_world->CreateJoint(myjoint);
    }
  
/*!Horizontal rotating shelf2 level 3(contains sphere) |
*b2PolygonShape is used to create a rod |
*Rod width is 4.0 and height 0.3 |
*Rod is fixed at (32.5, 19.5) and rotates about that point 
*/
  {

    b2Body* b2;
    {
      b2PolygonShape shape;
      shape.SetAsBox(4.0f, 0.30f);

      b2FixtureDef fd;
      fd.shape = &shape;
      fd.density = 60.0f;
      fd.friction = 0.5f;

      b2BodyDef bd;
      bd.type = b2_dynamicBody;
      bd.position.Set(32.5f, 19.5f);
      b2 = m_world->CreateBody(&bd);
      b2->CreateFixture(&shape, 10.0f);
    }


    b2Body* sbody;
    b2CircleShape circle;
    circle.m_radius = 0.30;

    b2FixtureDef ballfd;
    ballfd.shape = &circle;
    ballfd.density = 50.0f;
    ballfd.friction = 0.0f;
    ballfd.restitution = 0.0f;
    b2BodyDef ballbd;

    ballbd.position.Set(32.5f, 19.5f);
    sbody = m_world->CreateBody(&ballbd);
    sbody->CreateFixture(&ballfd);



    b2RevoluteJointDef jd;
    b2Vec2 anchor;
    anchor.Set(32.5f, 19.5f);
    jd.Initialize(sbody, b2, anchor);
    m_world->CreateJoint(&jd);

  }

/*! Ball on the rotating platform |
*b2CircleShape is used to create a sphere |
*Sphere radius is 1.0 and density is 50.0 |
*Sphere position is intially set to (32.5, 20.5) 
*/
  {
    b2Body* sbody;
    b2CircleShape circle;
    circle.m_radius = 1.0;

    b2FixtureDef ballfd;
    ballfd.shape = &circle;
    ballfd.density = 50.0f;
    ballfd.friction = 0.0f;
    ballfd.restitution = 0.0f;
    b2BodyDef ballbd;
    ballbd.type = b2_dynamicBody;
    ballbd.position.Set(32.5f, 20.5f);
    sbody = m_world->CreateBody(&ballbd);
    sbody->CreateFixture(&ballfd);
  }

 // Curved Surfaces
  float cxc = 39.0; 
  float cyc = 15.0;
/*! Curves are made using a fixed position |
*Radius is selected to 4 and Quarter Circle is made 
*/
{///First Curve
 b2BodyDef bd;
 bd.position.Set(cxc, cyc);
 b2Body* m_body;
 m_body = m_world->CreateBody(&bd);

 b2EdgeShape shape;

 b2Vec2 p1, p2;
 int m_nBorderVertices = 1000;
 float radiusc = -4.0;
 float xc , yc;  

 for (float i=0; i<m_nBorderVertices-1; i++) {
  xc = radiusc* sin ((i/1000)*PI/2);
  yc = radiusc* cos ((i/1000)*PI/2);
  p1 = b2Vec2(xc, yc);
  xc = radiusc* sin (((i+1)/1000)*PI/2);
  yc = radiusc* cos (((i+1)/1000)*PI/2);
  p2 = b2Vec2(xc, yc);
  shape.Set(p1, p2);
  m_body->CreateFixture(&shape, 0);
}
b2BodyDef bd1;
bd1.position.Set(cxc+2, cyc-2);
b2Body* m_body1;
m_body1 = m_world->CreateBody(&bd1);

b2EdgeShape shape1;

///Second curve
radiusc =-4.0;

for (float i=0; i<m_nBorderVertices-1; i++) {
  xc = radiusc* sin (-(i/1000)*PI/2);
  yc = radiusc* cos (-(i/1000)*PI/2);
  p1 = b2Vec2(xc, yc);
  xc = radiusc* sin (-((i+1)/1000)*PI/2);
  yc = radiusc* cos (-((i+1)/1000)*PI/2);
  p2 = b2Vec2(xc, yc);
  shape1.Set(p1, p2);
  m_body1->CreateFixture(&shape1, 0);
}




///Third curve
b2BodyDef bd2;
bd2.position.Set(cxc+2, cyc-6);
b2Body* m_body2;
m_body2 = m_world->CreateBody(&bd2);

b2EdgeShape shape2;


for (float i=0; i<m_nBorderVertices-1; i++) {
  xc = radiusc* sin ((i/1000)*PI/2);
  yc = radiusc* cos ((i/1000)*PI/2);
  p1 = b2Vec2(xc, yc);
  xc = radiusc* sin (((i+1)/1000)*PI/2);
  yc = radiusc* cos (((i+1)/1000)*PI/2);
  p2 = b2Vec2(xc, yc);
  shape2.Set(p1, p2);
  m_body2->CreateFixture(&shape2, 0);
}

///Curve attached to ground below it
b2BodyDef bd3;
bd3.position.Set(cxc+4, cyc-10);
b2Body* m_body3;
m_body3 = m_world->CreateBody(&bd3);

b2EdgeShape shape3;


radiusc =-4.0;

for (float i=0; i<m_nBorderVertices-1; i++) {
  xc = radiusc* sin (-(i/1000)*PI/2);
  yc = radiusc* cos (-(i/1000)*PI/2);
  p1 = b2Vec2(xc, yc);
  xc = radiusc* sin (-((i+1)/1000)*PI/2);
  yc = radiusc* cos (-((i+1)/1000)*PI/2);
  p2 = b2Vec2(xc, yc);
  shape3.Set(p1, p2);
  m_body3->CreateFixture(&shape3, 0);
}

}

/*!Inclined ground plank |
*An inclined plank is made using b2EdgeShape |
*Line Segment exists from (-16, -0.5) to (41, 1)
*/
{

  b2EdgeShape shape; 
  shape.Set(b2Vec2(cxc-55, cyc-15.5), b2Vec2(cxc+4, cyc-14));
  b2BodyDef bd; 
  b1 = m_world->CreateBody(&bd); 
  b1->CreateFixture(&shape, 0.0f);


}


cxc=cxc-5;

///Next six Shapes builds the box around piston
{

  b2Body* b2;
  b2PolygonShape shape;
  shape.SetAsBox(4.0f, 0.25f);

  b2FixtureDef fd;
  fd.shape = &shape;
  fd.density = 60.0f;
  fd.friction = 0.5f;

  b2BodyDef bd;
  bd.position.Set(cxc-54, cyc-16);
  b2 = m_world->CreateBody(&bd);
  b2->CreateFixture(&shape, 10.0f);


}


{

  b2Body* b2;
  b2PolygonShape shape;
  shape.SetAsBox(4.0f, 0.25f);

  b2FixtureDef fd;
  fd.shape = &shape;
  fd.density = 60.0f;
  fd.friction = 0.5f;

  b2BodyDef bd;
  bd.position.Set(cxc-54, cyc-10);
  b2 = m_world->CreateBody(&bd);
  b2->CreateFixture(&shape, 10.0f);


}
{

  b2Body* b2;
  b2PolygonShape shape;
  shape.SetAsBox(0.25f, 1.25f);

  b2FixtureDef fd;
  fd.shape = &shape;
  fd.density = 60.0f;

  b2BodyDef bd;
  bd.position.Set(cxc-58, cyc-15);
  b2 = m_world->CreateBody(&bd);
  b2->CreateFixture(&shape, 10.0f);



}
{

  b2Body* b2;
  b2PolygonShape shape;
  shape.SetAsBox(0.25f, 1.25f);

  b2FixtureDef fd;
  fd.shape = &shape;
  fd.density = 60.0f;

  b2BodyDef bd;
  bd.position.Set(cxc-58, cyc-11);
  b2 = m_world->CreateBody(&bd);
  b2->CreateFixture(&shape, 10.0f);

}
{

  b2Body* b2;
  b2PolygonShape shape;
  shape.SetAsBox(2.0f, 0.25f);

  b2FixtureDef fd;
  fd.shape = &shape;
  fd.density = 60.0f;

  b2BodyDef bd;
  bd.position.Set(cxc-60, cyc-12);
  b2 = m_world->CreateBody(&bd);
  b2->CreateFixture(&shape, 10.0f);



}
{

  b2Body* b2;
  b2PolygonShape shape;
  shape.SetAsBox(2.0f, 0.25f);

  b2FixtureDef fd;
  fd.shape = &shape;
  fd.density = 60.0f;

  b2BodyDef bd;
  bd.position.Set(cxc-60, cyc-14);
  b2 = m_world->CreateBody(&bd);
  b2->CreateFixture(&shape, 10.0f);



}

///Piston like structure with two rectangle boxes with weldJoint
{

  b2Body* b2;
  b2PolygonShape shape;
  shape.SetAsBox(1.0f, 2.5f);

  b2FixtureDef fd;
  fd.shape = &shape;
  fd.density = 0.5f;

  b2BodyDef bd;
  bd.type = b2_dynamicBody;
  bd.position.Set(cxc-52, cyc-13);
  b2 = m_world->CreateBody(&bd);
  b2->CreateFixture(&shape, 10.0f);

  {
    b2Body* b3;
    b2PolygonShape shape1;
    shape1.SetAsBox(4.0f, 0.5f);

    b2FixtureDef fd1;
    fd1.shape = &shape;
    fd1.density = 0.5f;

    b2BodyDef bd1;
    bd1.type = b2_dynamicBody;
    bd1.position.Set(cxc-56, cyc-13);
    b3 = m_world->CreateBody(&bd1);
    b3->CreateFixture(&shape1, 10.0f);


    b2WeldJointDef      weldJointDef;

    b2Vec2 anchor1;
    anchor1.Set(cxc-52, cyc-13);
    weldJointDef.Initialize(b2,b3,anchor1);
    m_world->CreateJoint(&weldJointDef);



  }


  {
/*!Block attached to balloon |
*b2PolygonShape is used to generate a box |
*Width is 1.25 and Height is 0.6 with density 0.5
*/
    b2Body* b2;
    {
      b2PolygonShape shape;
      shape.SetAsBox(1.25f, 0.60f);

      b2FixtureDef fd;
      fd.shape = &shape;
      fd.density = 0.5f;
      fd.friction = 0.0f;

      b2BodyDef bd;
      bd.type = b2_dynamicBody;
      bd.position.Set(cxc-61, cyc-12.75);
      b2 = m_world->CreateBody(&bd);
      b2->CreateFixture(&shape, 10.0f);
      b2->SetGravityScale(0.1f);
    }
/*!Thread attached to balloon |
*Thread is made from b2PolygonShape |
*It's width is 0.05 and height is 3.0
*/
    b2Body* b3;
    {
      b2PolygonShape shape;
      shape.SetAsBox(0.050f, 3.0f);

      b2FixtureDef fd;
      fd.shape = &shape;
      fd.density = 0.5f;
      fd.friction = 0.0f;

      b2BodyDef bd;
      bd.type = b2_dynamicBody;
      bd.position.Set(cxc-62, cyc-9.75);
      b3 = m_world->CreateBody(&bd);
      b3->CreateFixture(&shape, 10.0f);
      b3->SetGravityScale(-0.1f);
    }
    b2WeldJointDef      weldJointDef;

    b2Vec2 anchor1;
    anchor1.Set(cxc-62, cyc-12.75);
    weldJointDef.Initialize(b2,b3,anchor1);
    m_world->CreateJoint(&weldJointDef);

/*!Balloon |
*b2CircleShape is used to sphere |
*Radius of sphere/balloon is 3.0 and density is 0.5 |
*Gravity for that balloon is set in upward direction with a value of 0.5g 
*/
    b2Body* sb;
    {

      b2CircleShape circle;
      circle.m_radius = 3.0;

      b2FixtureDef ballfd;
      ballfd.shape = &circle;
      ballfd.density = 0.5f;
      ballfd.friction = 0.0f;
      ballfd.restitution = 0.0f;
      b2BodyDef ballbd;
      ballbd.type = b2_dynamicBody;
      ballbd.position.Set(cxc-62, cyc-3.75);
      sb = m_world->CreateBody(&ballbd);
      sb->CreateFixture(&ballfd);
      sb->SetGravityScale(-0.5f);
    }
    b2WeldJointDef      weldJointDef1;

    b2Vec2 anchor2;
    anchor2.Set(cxc-62, cyc-3.75);
    weldJointDef1.Initialize(b3,sb,anchor2);
    m_world->CreateJoint(&weldJointDef1);
    


  }



}




}



sim_t *sim = new sim_t("Dominos", dominos_t::create);
}
